<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllMessageLogsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('incoming');
            $table->text('body');
            $table->string('participant_phone');
            $table->string('service_phone');
            $table->string('message_sid')->nullable();
            $table->unsignedInteger('participant_id');
            $table->unsignedInteger('campaign_id')->nullable();
            $table->unsignedInteger('logician_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamp('timestamp', 6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_logs');
    }
}
