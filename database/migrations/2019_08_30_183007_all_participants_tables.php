<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllParticipantsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_phone');
            $table->string('service_phone');
            $table->string('logician_id')->nullable();
            $table->string('logician_method')->nullable();
            $table->string('current_campaign_id')->nullable();
            $table->string('current_trigger_id')->nullable();
            $table->timestamps();
        });

        Schema::create('participant_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('participant_id');
            $table->unsignedInteger('campaign_id');
            $table->string('name')->nullable();
            $table->string('key');
            $table->text('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connectors');
    }
}
