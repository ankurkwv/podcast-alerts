<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPodcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podcasts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('listen_notes_id');
            $table->unsignedInteger('organization_id');
            $table->timestamps();
        });

        Schema::create('podcast_episodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('listen_notes_id');
            $table->timestamp('pub_date');
            $table->unsignedInteger('podcast_id');
            $table->timestamps();
        });

        Schema::create('podcast_participant_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('podcast_id');
            $table->unsignedInteger('participant_id');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });

        Schema::create('participant_episode_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('message_log_id')->nullable();
            $table->unsignedInteger('participant_id');
            $table->unsignedInteger('episode_id');
            $table->unsignedInteger('podcast_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
