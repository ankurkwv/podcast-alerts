<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllCampaignTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedInteger('organization_id');
            $table->unsignedInteger('fallback_logician_id');
            $table->timestamps();
        });

        Schema::create('triggers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trigger');
            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('logician_id');
        });

        Schema::create('logicians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('driver');
        });

        Schema::create('trigger_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('trigger_id');
            $table->string('name')->nullable();
            $table->string('key');
            $table->text('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
