<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opt_in_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('notes');
            $table->string('participant_phone');
            $table->string('service_phone');
            $table->unsignedInteger('participant_id');
            $table->unsignedInteger('organization_id');
            $table->string('incoming_message_sid')->nullable();
            $table->string('confirmation_message_sid')->nullable();
            $table->timestamp('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
