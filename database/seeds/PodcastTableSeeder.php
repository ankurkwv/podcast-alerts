<?php

use Illuminate\Database\Seeder;

class PodcastTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('podcasts')->delete();
        
        \DB::table('podcasts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'My First Million',
                'listen_notes_id' => '72da7d9bd0474055a7f3fe523c6def1d',
                'organization_id' => 1
            ),
        ));
        
        
    }
}