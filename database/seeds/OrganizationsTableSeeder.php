<?php

use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('organizations')->delete();
        
        \DB::table('organizations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'The Hustle',
                'service_number' => '+14157022828',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}