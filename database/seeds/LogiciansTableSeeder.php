<?php

use Illuminate\Database\Seeder;

class LogiciansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('logicians')->delete();
        
        \DB::table('logicians')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Fallback Logician',
                'driver' => 'fallback',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Podcast Reminder',
                'driver' => 'podcast_reminder',
            )
        ));
        
        
    }
}