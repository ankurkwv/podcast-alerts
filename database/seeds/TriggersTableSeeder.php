<?php

use Illuminate\Database\Seeder;

class TriggersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('triggers')->delete();
        
        \DB::table('triggers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'trigger' => 'MILLION',
                'campaign_id' => 1,
                'logician_id' => 2,
            )
        ));
        

        \DB::table('trigger_settings')->delete();
        
        \DB::table('trigger_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'trigger_id' => 1,
                'key' => 'podcast_id',
                'value' => 1,
            )
        ));
        
        
    }
}