<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CampaignsTableSeeder::class);
        $this->call(LogiciansTableSeeder::class);
        $this->call(OrganizationsTableSeeder::class);
        $this->call(TriggersTableSeeder::class);
        $this->call(PodcastTableSeeder::class);
    }
}
