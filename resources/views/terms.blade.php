<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bot - Terms & Privacy</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
      body{
        background:#5F6CAF;
      }
      #main-area{
        margin:auto;
        margin-top:80px;
        margin-bottom:100px;
        width: 90%;
        max-width: 800px;
        font-family: 'Open Sans', sans-serif;
        color:#fff;line-height: 1.5;
      }
      h2{
         font-size: 2em;
      }
      p{
        font-size: 1.2em;
      }
      a{
        color:#EDF7FA;
      }
      @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
		#main-area {
			margin-top: 40px;
		}
      }
    </style>
  </head>
  <body>
    <div id="main-area">
      <h2>Podcast Alert Bot – Terms of Service</h2>
      <p>1. When you opt-in to the service, we will send you an SMS message to confirm your signup. You can expect messages pertaining to reminders to vote in local, state, and/or national elections pertinent to you.</p>
      <p>2. You can cancel the SMS service at any time. Just text "STOP" to the service number . After you send the SMS message "STOP" to us, we will send you an SMS message to confirm that you have been unsubscribed. After this, you will no longer receive SMS messages from us.</p>
      <p>If you want to join again, just sign up as you did the first time and we will start sending SMS messages to you again.</p>
      <p>3. If at any time you forget what keywords are supported, just text "HELP" to the service number . After you send the SMS message "HELP" to us, we will respond with instructions on how to use our service as well as how to unsubscribe.</p>
      <p>4. We are able to deliver messages to the following mobile phone carriers: Major carriers: AT&T, Verizon Wireless, Sprint, T-Mobile, MetroPCS, U.S. Cellular, Alltel, Boost Mobile, Nextel, and Virgin Mobile.</p>
      <p>Minor carriers: Alaska Communications Systems (ACS), Appalachian Wireless (EKN), Bluegrass Cellular, Cellular One of East Central IL (ECIT), Cellular One of Northeast Pennsylvania, Cincinnati Bell Wireless, Cricket, Coral Wireless (Mobi PCS), COX, Cross, Element Mobile (Flat Wireless), Epic Touch (Elkhart Telephone), GCI, Golden State, Hawkeye (Chat Mobility), Hawkeye (NW Missouri), Illinois Valley Cellular, Inland Cellular, iWireless (Iowa Wireless), Keystone Wireless (Immix Wireless/PC Man), Mosaic (Consolidated or CTC Telecom), Nex-Tech Wireless, NTelos, Panhandle Communications, Pioneer, Plateau (Texas RSA 3 Ltd), Revol, RINA, Simmetry (TMP Corporation), Thumb Cellular, Union Wireless, United Wireless, Viaero Wireless, and West Central (WCC or 5 Star Wireless).</p>
      <p>***Carriers are not liable for delayed or undelivered messages***</p>
      <p>5. As always, message and data rates may apply for any messages sent to you from us and to us from you. Message frequency will vary. If you have any questions about your text plan or data plan, it is best to contact your wireless provider.</p>
      <p>For all questions about the services provided by this short code, you can send an email to bot@oakwood.digital. We're here to help and will respond as soon as possible.</p>
      <p>6. If you have any questions regarding privacy, please read our privacy policy: <a href="/privacy.html">Privacy Policy</a></p>
      <h2>Podcast Alert Bot – Privacy Policy</h2>
      The Podcast Alert Bot application and the associated website available at <a href="https://podcastalerts.oakwood.digital/">https://podcastalerts.oakwood.digital</a> (“Podcast Alert Bot”) are owned and operated by Oakwood Technology LLC ("us" "we" or "our"). This Podcast Alert Bot privacy policy (the "Privacy Policy") is intended to inform you of our policies and practices regarding the collection, use and disclosure of any information you submit to us through Podcast Alert Bot. This includes "Personal Information," which is information about you that is personally identifiable such as your name, e-mail address, user ID number, and other non-public information that is associated with the foregoing, as well as "Anonymous Information," which is information that is not associated with or linked to your Personal Information and does not permit the identification of individual persons.

      <h3>User Consent</h3>

      <p>By accessing or otherwise using Podcast Alert Bot, you agree to the terms and conditions of this Privacy Policy and the associated Terms of Service (set forth on <a href="/terms.html">https://podcastalerts.oakwood.digital/terms.html</a> you expressly consent to the processing of your Personal Information and Anonymous Information according to this Privacy Policy.</p>
      <p>Your Personal Information may be processed by us in the country where it was collected as well as other countries (including the United States) where laws regarding processing of Personal Information may be less stringent than the laws in your country.</p>

      <h3>Regarding Children</h3>

      <p>Children under the age of 13 are not permitted to use Podcast Alert Bot and we do not intentionally collect or maintain Personal Information from those who are under 13 years old.</p>
      <p>Protecting the privacy of children is very important to us. Thus, if we obtain actual knowledge that a user is under 13, we will take steps to remove that user's Personal Information from our databases. We recommend that children between the ages of 13 and 18 obtain their parent's permission before submitting information over the internet. By using Podcast Alert Bot, you are representing that you are at least 18 years old, or that you are at least 13 years old and have your parents' permission to use Podcast Alert Bot.</p>

      <h3>Collection and Use of Information</h3>

      <h3>Personal Information</h3>

      <p>In general, we collect Personal Information that you submit to us voluntarily through Podcast Alert Bot. For example, our bot usage and process requires you to provide us with your name and phone number. We will collect any information you voluntarily provide, and we may also request optional information to support your use of Podcast Alert Bot.We collect information in the form of the content that you submit during your use of Podcast Alert Bot, such as comments, messages, and other information you choose to submit. When you order our products or services, you will need to submit your credit card or other payment information so that our service providers can process your payment for those products and services. If you choose to sign up to receive information about products or services that may be of interest to you, we will collect your email address and all related information. Additionally, we collect any information that you voluntarily enter, including Personal Information, into any postings, comments, or forums within the Podcast Alert Bot community.</p>

      <h3>E-mail and E-mail Addresses</h3>

      <p>If you send an e-mail to us, or fill out our "Feedback" form through Podcast Alert Bot, we will collect your e-mail address and the full content of your e-mail, including attached files, and other information you provide. We may use and display your full name and email address when you send an email notification to a friend through Podcast Alert Bot. </p>

      <h3>Information Collected Via Technology</h3>

      <p>As you use Podcast Alert Bot, certain information may also be passively collected and stored on our or our service providers' server logs, including your Internet protocol address, browser type, and operating system. We also use Cookies and navigational data like Uniform Resource Locators (URL) to gather information regarding the date and time of your visit and the solutions and information for which you searched and viewed, or on which of the advertisements displayed on Podcast Alert Bot you clicked. This type of information is collected to make Podcast Alert Bot and solutions more useful to you and to tailor the experience with Podcast Alert Bot to meet your special interests and needs. An "Internet protocol address" or "IP Address" is a number that is automatically assigned to your computer when you use the Internet. In some cases your IP Address stays the same from browser session to browser session; but if you use a consumer internet access provider, your IP Address probably varies from session to session. For example, we, or our service providers, may track your IP Address when you access Podcast Alert Bot to assist with ad targeting.</p>
      <p>"Cookies" are small pieces of information that a website sends to your computer's hard drive while you are viewing a website. We may use both session Cookies (which expire once you close your web browser) and persistent Cookies (which stay on your computer until you delete them) to provide you with a more personal and interactive experience with Podcast Alert Bot. Persistent Cookies can be removed by following your Internet browser help file directions. In order to use our services offered through Podcast Alert Bot, your web browser must accept Cookies. If you choose to disable Cookies, some aspects of Podcast Alert Bot may not work properly, and you will not be able to receive our services.</p>

      <h3>Use and Disclosure of Information</h3>

      <p>Except as otherwise stated in this Privacy Policy, we do not generally sell, trade, rent, or share the Personal Information that we collect with third parties, unless you ask or authorize us to do so.</p>
      <p>In general, Personal Information you submit to us is used by us to provide you access to Podcast Alert Bot, to improve Podcast Alert Bot, to better tailor the features, performance, and support of Podcast Alert Bot and to offer you additional information, opportunities, promotions and functionality from us, our partners or our advertisers at your request. </p>
      <p>We may provide your Personal Information to third-party service providers who work on behalf of or with us to provide some of the services and features of Podcast Alert Bot and to help us communicate with you. Examples of such services include sending text messages, sending email, analyzing data, providing marketing assistance, processing payments (including credit card payments), and providing customer service. We require our third-party service providers to promise not to use such information except as necessary to provide the relevant services to us. We may share some or all of your Personal Information with our third-party partners, those with whom we have a co-branding or promotional relationship, or other third-parties about whom you are otherwise notified and do not request to opt out of such sharing. This Privacy Policy does not cover the use of your personally identifiable information by such third-parties. We do not maintain responsibility for the manner in which third parties, including, without limitation, social networks, other partners and advertisers, use or further disclose Personal Information collected from you in accordance with this Privacy Policy, after we have disclosed such information to those third parties. If you do not want us to use or disclose Personal Information collected about you in the manners identified in this Privacy Policy, you may not use Podcast Alert Bot.</p>
      <p>Although we currently do not have a parent company, any subsidiaries, joint ventures, or other companies under a common control, we may in the future, and we may share some or all of your Personal Information with these companies, in which case we will require them to honor this Privacy Policy. In the event we go through a business transition such as a merger, acquisition by another company, or sale of all or a portion of its assets, your Personal Information may be among the assets transferred. You acknowledge that such transfers may occur and are permitted by this Privacy Policy, and that any acquirer of ours or that acquirer's assets may continue to process your Personal Information as set forth in this Privacy Policy.</p>
      <p>We may disclose your Personal Information if we believe in good faith that such disclosure is necessary to (a) comply with relevant laws or to respond to subpoenas or warrants served on us; or (b) to protect and defend our rights or property, you, or third parties. You hereby consent to us sharing your Personal Information under the circumstances described herein.</p>

      <h3>The Ability of others to View your Information</h3>

      <p>Helping you to protect your information is a vital part of our mission. It is up to you to make sure you are comfortable with the information you choose to provide us and the information you choose to publish. You understand that when you use Podcast Alert Bot, certain information you post or provide through Podcast Alert Bot, such as your name and phone number, may be shared with system administrators and posted on admin-only portions of Podcast Alert Bot, including without limitation, chatrooms, bulletin and message boards, along with other private forums. Please keep in mind that if you choose to disclose Personal Information when posting comments or other information or content through Podcast Alert Bot, this information may become publicly available and may be collected and used by others, including people outside the Podcast Alert Bot community.</p>
      <p>We will not have any obligations with respect to any information that you post to parts of Podcast Alert Bot available to others, and recommend that you use caution when giving out personal information to others in public forums online or otherwise. We also share the information you publish with other third parties.</p>

      <h3>Third Party Sites and Advertising</h3>

      <p>Podcast Alert Bot may contain links to other websites. Please be aware that we are not responsible for the privacy practices or the content of such other websites. We encourage our users to read the privacy statements of each and every website they visit. This Policy applies solely to information collected by us through Podcast Alert Bot and does not apply to these third-party websites. The ability to access information of third-parties from Podcast Alert Bot, or links to other websites or locations, is for your convenience and does not signify our endorsement of such third-parties, their products, their services, other websites, locations or their content.</p>

      <h3>Your Choices Regarding Your Personal Information</h3>

      <p>We offer you choices regarding the collection, use, and sharing of your Personal Information. When you receive promotional communications from us, you may indicate a preference to stop receiving further promotional communications from us and you will have the opportunity to "opt-out" by following the unsubscribe instructions provided in the promotional e-mail you receive or by contacting us directly (please see contact information below). </p>
      <p>Despite your indicated email preferences, we may send you administrative emails regarding Podcast Alert Bot, including, for example, administrative confirmations, and notices of updates to our Privacy Policy if we choose to provide such notices to you in this manner. You may change any of your profile information by editing it in the profile settings page. You may request deletion of your Personal Information by contacting us at bot@oakwood.digital, but please note that we may be required (by law or otherwise) to keep this information and not delete it (or to keep this information for a certain time, in which case we will comply with your deletion request, only after we have fulfilled such requirements). When we delete Personal Information, it will be deleted from the active database, but may remain in our archives.</p>

      <h3>Feedback</h3>

      <p>If you provide feedback to us, we may use and disclose such feedback for any purpose, provided we do not associate such feedback with your Personal Information. We will collect any information contained in such feedback and will treat the Personal Information in it in accordance with this Privacy Policy. You agree that any such comments and any email we receive becomes our property. We may use feedback for marketing purposes or to add to or modify our services without paying any royalties or other compensation to you.</p>

      <h3>Security</h3>

      <p>We are committed to protecting the security of your Personal Information. We use a variety of industry-standard security technologies and procedures to help protect your Personal Information from unauthorized access, use, or disclosure. Even though we have taken significant steps to protect your Personal Information, no company, including us, can fully eliminate security risks associated with Personal Information.</p>

      <h3>Contact and Revisions</h3>

      <p>If you have questions or concerns about our Privacy Policy, please contact us at: bot@oakwood.digital. This Privacy Policy is subject to occasional revision at our discretion, and if we make any substantial changes in the way we use your Personal Information, we will post an alert on this page. If you object to any such changes, you must cease using Podcast Alert Bot. Continued use of Podcast Alert Bot following notice of any such changes shall indicate your acknowledgement of such changes and agreement to be bound by the terms and conditions of such changes.</p>

    </div>

  </body>
</html>