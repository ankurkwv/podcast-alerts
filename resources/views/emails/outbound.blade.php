@component('mail::message')
# New Connection
{{ $connectorEmail }}

{{ $body }}

Powered by [simpleconnect.io](https://simpleconnect.io)
@endcomponent
