<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Podcast Alerts</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
      body{
        background:#5F6CAF;
      }
      #main-area{
        margin:auto;
        margin-top:80px;
        margin-bottom:100px;
        width: 90%;
        max-width: 800px;
        font-family: 'Open Sans', sans-serif;
        color:#fff;line-height: 1.5;
      }
      h2{
         font-size: 2em;
      }
      p{
        font-size: 1.2em;
      }
      a{
        color:#EDF7FA;
      }
      @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
    #main-area {
      margin-top: 40px;
    }
      }
    </style>
  </head>
  <body>
    <div id="main-area">
      <h2>Podcast Alerts</h2>
        <p>
          Welcome to Podcast Alerts. See <a href="/terms-privacy">terms of service and privacy policy</a>.
        </p>
        <p>
          If you have any questions regarding our services please contact us at <b>bot@oakwood.digital</b> or toll-free at <b>+1 (855) 923-1465.</b>
        </p>
    </div>

  </body>
</html>