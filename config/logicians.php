<?php

return [

    'global_fallback' => env('LOGICIAN_GLOBAL_FALLBACK', 'fallback'),
    'fallback_message' => 'Welcome to Podcast Alerts. Msg frequency will vary, msg&data rates apply. Reply HELP for help, STOP to stop.',
    'twilio_account' => env('TWILIO_SID'),
    'twilio_token' => env('TWILIO_TOKEN')

];
