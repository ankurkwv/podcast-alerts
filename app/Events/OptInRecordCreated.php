<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\OptInRecord;

class OptInRecordCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $optIn;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(OptInRecord $optIn)
    {
        $this->optIn = $optIn;
    }
}
