<?php

namespace App\Mail;

use App\Connector;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InboundConnect extends Mailable
{
    use Queueable, SerializesModels;

    public $connectionEmail;
    public $connectorEmail;
    public $notes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Connector $connector, $notes)
    {
        $this->connectionEmail = $connector->latest_contact;
        $this->connectorEmail = $connector->user_email;
        $this->notes = $notes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.inbound')
                    ->subject('New Connection: ' . $this->connectionEmail)
                    ->to($this->connectorEmail)
                    ->replyTo($this->connectionEmail);
    }
}
