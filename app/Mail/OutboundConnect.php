<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Connector;

class OutboundConnect extends Mailable
{
    use Queueable, SerializesModels;
    
    public $connectionEmail;
    public $connectorEmail;
    public $connectorName;
    public $body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($connection_email, Connector $connector)
    {
        $this->connectionEmail = $connection_email;
        $this->connectorName = $connector->user_name;
        $this->body = $connector->user_message;
        $this->connectorEmail = $connector->user_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.outbound')
                    ->subject('New Connection: ' . $this->connectorName)
                    ->to($this->connectionEmail)
                    ->replyTo($this->connectorEmail);
    }
}
