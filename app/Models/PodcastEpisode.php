<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PodcastEpisode extends Model
{
    protected $guarded = ['id'];
    protected $table = 'podcast_episodes';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function podcast()
    {
    	return $this->belongsTo(Podcast::class);
    }
    
    public function notifications()
    {
    	return $this->hasMany(PodcastNotification::class);
    }
    
    /*=====  End of Relationships  ======*/
    
}
