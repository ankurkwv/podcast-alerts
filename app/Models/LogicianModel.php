<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogicianModel extends Model
{
    protected $guarded = ['id'];
    protected $table = 'logicians';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
	public function triggerSettings()
	{
		return $this->hasMany(LogicianTriggerSetting::class);
	}

	public function triggers()
	{
		return $this->hasMany(Trigger::class);
	}
	
    /*=====  End of Relationships  ======*/
    
}
