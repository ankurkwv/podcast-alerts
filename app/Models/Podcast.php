<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Podcast extends Model
{
    protected $guarded = ['id'];
    protected $table = 'podcasts';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function organization()
    {
    	return $this->belongsTo(Organization::class);
    }
    
    public function episodes()
    {
        return $this->hasMany(PodcastEpisode::class);
    }

    public function subscriptions()
    {
    	return $this->hasMany(PodcastSubscription::class);
    }
    
    public function notifications()
    {
    	return $this->hasMany(PodcastNotification::class);
    }
    
    /*=====  End of Relationships  ======*/

    /*=================================
    =            Functions            =
    =================================*/

    public function latestEpisode()
    {
        return $this->episodes()->orderBy('pub_date', 'desc')->first();
    }
    
    public function subscribe(Participant $participant)
    {
        $subscription = PodcastSubscription::firstOrCreate([
            'podcast_id' => $this->id,
            'participant_id' => $participant->id,
        ]);

        // We'll go ahead and bring our new subscriber up to
        // date because there's no point in texting them just
        // minutes after they register that there's a new
        // episode.
        
        if ($episode = $this->latestEpisode()) {
            PodcastNotification::create([
                'podcast_id' => $this->id,
                'episode_id' => $episode->id,
                'participant_id' => $participant->id,
            ]);
        }

        return $subscription;
    }
    
    public function unsubscribe(Participant $participant)
    {
        $subscription = PodcastSubscription::where([
            'podcast_id' => $this->id,
            'participant_id' => $participant->id,
        ])->first();
        
        if ($subscription) {
            $subscription->active = 0;
            $subscription->save();
        }

        return $subscription;
    }
    
    /*=====  End of Functions  ======*/
}
