<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TriggerSetting extends Model
{
    protected $guarded = ['id'];
    protected $table = 'trigger_settings';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function trigger()
    {
    	return $this->belongsTo(Trigger::class);
    }
    
    /*=====  End of Relationships  ======*/
    
}
