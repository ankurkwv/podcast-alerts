<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trigger extends Model
{
    protected $guarded = ['id'];
    protected $table = 'triggers';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function logician()
    {
    	return $this->belongsTo(LogicianModel::class);
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function settings()
    {
    	return $this->hasMany(TriggerSetting::class);
    }
    
    /*=====  End of Relationships  ======*/
    
}
