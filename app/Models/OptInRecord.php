<?php

namespace App\Models;

use App\Events\OptInRecordCreated;
use Illuminate\Database\Eloquent\Model;

class OptInRecord extends Model
{
    protected $guarded = ['id'];
    protected $table = 'opt_in_records';
    public $timestamps = false;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => OptInRecordCreated::class,
    ];

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function participant()
    {
    	return $this->belongsTo(Participant::class, 'participant_id');
    }
    public function org()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public static function generateFromTrigger(Trigger $trigger, Participant $participant, $message_sid)
    {
        OptInRecord::firstOrCreate([
            'participant_phone' => $participant->user_phone,
            'service_phone' => $trigger->campaign->organization->service_number,
            'organization_id' => $trigger->campaign->organization->id,
            'participant_id' => $participant->id,
        ],  [
            'notes' => '[{"trigger": "'.$trigger->trigger.'"},{"campaign_id":"'.$trigger->campaign_id.'"}]',
            'incoming_message_sid' => $message_sid
        ]);
    }
    
    /*=====  End of Relationships  ======*/
    
}
