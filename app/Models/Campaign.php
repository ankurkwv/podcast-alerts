<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $guarded = ['id'];
    protected $table = 'campaigns';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function organization()
    {
    	return $this->belongsTo(Organization::class);
    }

    public function triggers()
    {
    	return $this->hasMany(Trigger::class);
    }
    
    public function fallbackLogician()
    {
    	return $this->belongsTo(LogicianModel::class, 'fallback_logician_id');
    }
    
    /*=====  End of Relationships  ======*/
    
}
