<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PodcastSubscription extends Model
{
    protected $guarded = ['id'];
    protected $table = 'podcast_participant_subscriptions';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function podcast()
    {
    	return $this->belongsTo(Podcast::class);
    }

    public function participant()
    {
    	return $this->belongsTo(Participant::class);
    }
    
    /*=====  End of Relationships  ======*/
    
    
}
