<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParticipantAttribute extends Model
{
    protected $guarded = ['id'];
    protected $table = 'participant_attributes';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function participant() {
    	return $this->belongsTo(Participant::class);
    }
    
    public function campaign() {
    	return $this->belongsTo(Campaign::class);
    }
    
    /*=====  End of Relationships  ======*/
    
}
