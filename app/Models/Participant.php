<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $guarded = ['id'];
    protected $table = 'participants';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function currentLogician()
    {
    	return $this->belongsTo(LogicianModel::class, 'logician_id');
    }

    public function currentCampaign()
    {
    	return $this->belongsTo(Campaign::class, 'current_campaign_id');
    }

    public function currentTrigger()
    {
        return $this->belongsTo(Trigger::class, 'current_trigger_id');
    }

    public function attributes()
    {
    	return $this->hasMany(ParticipantAttribute::class);
    }
    
    /*=====  End of Relationships  ======*/

    public function getParticipantAttribute($key, $campaign_id)
    {
        return $this->attributes()
            ->where('campaign_id', $campaign_id)
            ->where('key', $key)
            ->pluck('value')
            ->first();
    }
    	

}
