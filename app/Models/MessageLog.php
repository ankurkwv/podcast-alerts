<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageLog extends Model
{
    protected $guarded = ['id'];
	protected $table = 'message_logs';
    public $timestamps = false;

	/*=====================================
	=            Relationships            =
	=====================================*/
	
    public function particpant()
    {
    	return $this->belongsTo(Particpant::class);
    }

    public function campaign()
    {
    	return $this->belongsTo(Campaign::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function logician()
    {
    	return $this->belongsTo(LogicianModel::class);
    }

	/*=====  End of Relationships  ======*/
	
}
