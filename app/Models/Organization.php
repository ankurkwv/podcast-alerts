<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $guarded = ['id'];
    protected $table = 'organizations';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function users()
    {
    	return $this->hasMany(User::class);
    }
    
    /*=====  End of Relationships  ======*/
    		
}
