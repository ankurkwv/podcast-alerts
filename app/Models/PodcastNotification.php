<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PodcastNotification extends Model
{
    protected $guarded = ['id'];
    protected $table = 'participant_episode_notifications';

    /*=====================================
    =            Relationships            =
    =====================================*/
    
    public function podcast()
    {
    	return $this->belongsTo(Podcast::class);
    }

    public function episode()
    {
        return $this->belongsTo(Episode::class);
    }
    
    public function participant()
    {
    	return $this->hasMany(Participant::class);
    }
    
    public function message()
    {
        return $this->hasMany(MessageLog::class, 'message_log_id');
    }
    
    /*=====  End of Relationships  ======*/
    
}
