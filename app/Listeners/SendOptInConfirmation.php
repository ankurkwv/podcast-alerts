<?php

namespace App\Listeners;

use App\Events\OptInRecordCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Aloha\Twilio\Twilio;

class SendOptInConfirmation implements ShouldQueue
{
    public $tries = 1;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OptInRecordCreated  $event
     * @return void
     */
    public function handle(OptInRecordCreated $event)
    {
        return resolve('Logician')::driver('record_opt_in')->incoming($event->optIn);
    }
}
