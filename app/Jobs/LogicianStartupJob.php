<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Logicians\Facades\Logician;

class LogicianStartupJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $driver;
    private $data;
    private $method;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($driver, $data, $method = null)
    {
        $this->driver = $driver;
        $this->data = $data;
        $this->method = $method ?? 'incoming';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return Logician::driver($this->driver)
            ->initialize($this->data)
            ->{$this->method}($this->data);
    }
}
