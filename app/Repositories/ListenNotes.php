<?php

namespace App\Repositories;

use GuzzleHttp\Client as GuzzleClient;

use GuzzleHttp\HandlerStack;
use Carbon\Carbon;

class ListenNotes
{
    /**
     * The access token used for requests
     *
     * @var string
     */
    private $accessToken;

    /**
     * Set API base url
     *
     * @var string
     */
    private $baseUrl = 'https://listen-api.listennotes.com/api/v2';

    /**
     * The HTTP Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;


    public function __construct()
    {
        $this->setAccessToken(config('listennotes.access_token'));
    }

    /**
     * Get an instance of the Guzzle HTTP client.
     *
     * @return \GuzzleHttp\Client
     */
    private function getHttpClient()
    {
        if (is_null($this->httpClient)) {
            $this->httpClient = new GuzzleClient();
        }

        return $this->httpClient;
    }

    /**
     * Set accesst token
     *
     * @param string $token
     *
     * @return $this
     */
    private function setAccessToken($token)
    {
        $this->accessToken = $token;

        return $this;
    }

    /**
     * Get more info on a podcast
     *
     * @param integer $statementId
     *
     * @return array
     */
    public function getPodcast($podcast_id)
    {
        $url = '/podcasts/'.$podcast_id;

        return $this->request($url, 'get');
    }

    /**
     * Get more info on a episode
     *
     * @param integer $statementId
     *
     * @return array
     */
    public function getEpisode($episode_id)
    {
        $url = '/episodes/'.$episode_id;

        return $this->request($url, 'get');
    }

    /**
     * Undocumented function
     *
     * @param string $url
     * @param string $type
     * @param array $body
     *
     * @return array
     */
    private function request($url, $type)
    {
        $validTypes = ['get', 'post'];

        if (!in_array($type, $validTypes)) {
            throw new \Exception('Invalid request type');
        }

        $response = $this->getHttpClient()->{$type}($this->baseUrl . $url, [
            'headers' => [
                'X-ListenAPI-Key' => $this->accessToken
            ],
        ]);

        if ($response->getStatusCode() !== 200) {
            \Log::error('LISTEN NOTES ERROR ERROR');
            
            throw new \Exception('Could not retrieve podcast info.');
        }

        $response = json_decode($response->getBody(), true);

        return $response;
    }
}
