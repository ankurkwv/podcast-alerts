<?php

namespace App\Logicians\Facades;

use Illuminate\Support\Facades\Facade;
use App\Logicians\Contracts\Factory;

class Logician extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Factory::class;
    }
}