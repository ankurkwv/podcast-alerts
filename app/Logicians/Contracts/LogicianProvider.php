<?php

namespace App\Logicians\Contracts;

interface LogicianProvider
{
	/**
	 *
	 * Every Logician will have an incoming method
	 * to accept & process the sms data (from, to, and message).
	 * 	
	 */
	
    public function incoming($message_data);

    /**
     *
     * The initialize method will set the context
     * for our Logician provider (Twilio, etc)
     *
     * Must return instance of the provider!
     *
     */
    
    public function initialize($message_data);
    
}