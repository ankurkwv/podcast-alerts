<?php

namespace App\Logicians\Contracts;

interface Factory
{
    /**
     * Get a logician driver implementation.
     *s
     * @param  string  $driver
     *
     * @return \App\Logicians\Contracts\Provider
     */
    public function driver($driver = null);
}
