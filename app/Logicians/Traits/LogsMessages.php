<?php

namespace App\Logicians\Traits;

use App\Models\MessageLog;
use App\Models\Participant;
use App\Models\Campaign;
use App\Models\LogicianModel;

use DateTime; 

trait LogsMessages
{
	protected function logIncomingMessage($data, Campaign $campaign, Participant $participant, LogicianModel $logician) {
		$log = new MessageLog;
		$log->incoming = 1;
		$log->body = $data['message'];
		$log->participant_phone = $data['from'];
		$log->service_phone = $data['to'];
		$log->message_sid = $data['sid'];
		$log->participant_id = $participant->id;
		$log->campaign_id = $campaign->id;
		$log->logician_id = $logician->id;
		$log->timestamp = $this->miliTime($data['time']);
		$log->save();
	}
	
	protected function logOutgoingTwilioMessage($message, Participant $participant) {
		$log = new MessageLog;
		$log->incoming = 0;
		$log->body = $message->body;
		$log->participant_phone = $message->to;
		$log->service_phone = $message->from;
		$log->message_sid = $message->sid;
		$log->participant_id = $participant->id ?? null;
		$log->campaign_id = $participant->current_campaign_id ?? null;
		$log->logician_id = $participant->logician_id ?? null;
		$log->timestamp = $this->miliTime(microtime(true));
		$log->save();
		return $log;
	}

	private function miliTime($t) {
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$dt = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
		return $dt->format("Y-m-d H:i:s.u");
	}
}
