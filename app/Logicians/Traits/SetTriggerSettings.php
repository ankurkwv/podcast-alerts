<?php

namespace App\Logicians\Traits;
use App\Models\Trigger;

trait SetTriggerSettings
{
	protected function setSettings(Trigger $trigger) {
		$settings = array();
		$trigger->settings->each(function($setting) use (&$settings) {
			$settings[$setting->key] = $setting->value;
		});
		return $settings;
	}
}