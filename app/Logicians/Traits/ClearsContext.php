<?php

namespace App\Logicians\Traits;

trait ClearsContext
{
	protected function clearContext() {
		$this->participant->logician_id = null;
		$this->participant->current_trigger_id = null;
		$this->participant->logician_method = null;
		$this->participant->save();
	}
}