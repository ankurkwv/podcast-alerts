<?php

namespace App\Logicians\Traits;

use App\Models\Participant;

trait AssignsVariables
{
	private function assignVariables(Participant $participant, $trigger = null) {
		$this->participant = $participant;
		$trigger = $trigger ?: $participant->currentTrigger;
		$this->trigger = $trigger;
		$this->campaign = $trigger->campaign;
		$this->logician = $trigger->logician;
		$this->settings = $trigger->settings->toArray();
	}
}
