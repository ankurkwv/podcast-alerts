<?php

namespace App\Logicians;

use InvalidArgumentException;
use Illuminate\Support\Manager;
use App\Logicians\Contracts\Factory;

use App\Logicians\Providers\FallbackProvider;
use App\Logicians\Providers\OptInProvider;
use App\Logicians\Providers\PodcastReminderProvider;


class LogicianManager extends Manager implements Factory
{
    public function createPodcastReminderDriver()
    {
        return $this->buildProvider(PodcastReminderProvider::class);
    }

    public function createFallbackDriver()
    {
        return $this->buildProvider(FallbackProvider::class);
    }

    public function createRecordOptInDriver()
    {
        return $this->buildProvider(OptInProvider::class);
    }

    /**
     * Build provider instance.
     *
     * @param  string  $provider
     */
    public function buildProvider($provider)
    {
        return new $provider();
    }


    /**
     * Get the default driver name.
     *
     * @throws \InvalidArgumentException
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        throw new InvalidArgumentException('No driver was specified.');
    }
}
