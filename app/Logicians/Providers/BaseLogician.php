<?php

namespace App\Logicians\Providers;

use App\Logicians\Contracts\LogicianProvider;
use Aloha\Twilio\Twilio;
use Twilio\Exceptions\RestException as TwilioException;
use App\Logicians\Traits\LogsMessages;

use App\Models\Participant;

use Log;

abstract class BaseLogician implements LogicianProvider
{
    use LogsMessages;

	private $twilio;

	abstract public function incoming($message_data);

	public function initialize($data) {
		$this->twilio = new Twilio(
    		config('logicians.twilio_account'), 
    		config('logicians.twilio_token'), 
    		$data['to']
    	);
    	return $this;
	}

    protected function sendMessage($messages, Participant $participant) {
		if (!is_array($messages)) {
			$messages = array($messages);
		}

		foreach ($messages as $message) {
			try {
    			$message = $this->twilio->message($participant->user_phone, $message);
                $this->logOutgoingTwilioMessage($message, $participant);
                return $message;
    		}
    		catch (TwilioException $e) {
    			Log::error($e->getMessage());
    		}
		}
    }
}
