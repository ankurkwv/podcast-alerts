<?php

namespace App\Logicians\Providers;

use App\Logicians\Providers\BaseLogician;

class OptInProvider extends BaseLogician
{
	public function incoming($optInRecord)
	{
        $this->initialize(['to' => $optInRecord->service_phone]);
        $message = "Podcast Alerts. ";
        $message .= "Msg & Data Rates May Apply. ";
        $message .= "Msg Frequency Varies. ";
        $message .= "Reply HELP for help. ";
        $message .= "Reply STOP to STOP. ";
        $message .= "Terms at https://podcastalerts.oakwood.digital.";
        
        $proof = $this->sendMessage($message, $optInRecord->participant);

        $optInRecord->confirmation_message_sid = $proof->sid;
        return $optInRecord->save();
	}
}
