<?php

namespace App\Logicians\Providers;

use App\Logicians\Providers\BaseLogician;
use App\Models\Trigger;
use App\Models\Participant;
use App\Models\Podcast;
use App\Models\ParticipantAttribute;

use App\Logicians\Traits\AssignsVariables;
use App\Logicians\Traits\ClearsContext;
use App\Logicians\Traits\SetTriggerSettings;

use Log;

class PodcastReminderProvider extends BaseLogician
{
	use AssignsVariables, ClearsContext, SetTriggerSettings;

	private $campaign;
	private $logician;
	private $trigger;
	private $settings;
	private $participant;

	public function incoming($message_data)
	{
		$this->bootMessage($message_data);

		try {
			Podcast::findOrFail($this->settings['podcast_id'])
				->subscribe($this->participant);
			$message = "Thanks for opting in to " . $this->campaign->name . " episode release alerts. We'll text you when the next episode comes out.";
		}
		catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			$message = "Sorry, we couldn't process your request. Podcast not found.";
		}

		$this->clearContext();
		return $this->sendMessage($message, $this->participant);
	}

	private function bootMessage($data) {
		$this->assignVariables($data['participant'], $data['trigger']);
		$this->settings = $this->setSettings($this->trigger);
		$this->logIncomingMessage($data, $this->campaign, $this->participant, $this->logician);
	}
}
