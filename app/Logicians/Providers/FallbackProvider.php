<?php

namespace App\Logicians\Providers;

use App\Logicians\Providers\BaseLogician;

class FallbackProvider extends BaseLogician
{
	public function incoming($message_data)
	{
		$this->initialize($message_data);
		return $this->sendMessage(config('logicians.fallback_message'), $message_data['participant']);
	}
}
