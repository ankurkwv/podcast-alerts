<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearParticipantData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'participants:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all participant data (for testing).';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(env('APP_DEBUG')) {
            \App\Models\Participant::truncate();
            \App\Models\ParticipantAttribute::truncate();
            \App\Models\MessageLog::truncate();
            \App\Models\OptInRecord::truncate();
        }
    }
}
