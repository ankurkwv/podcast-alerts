<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ListenNotes;

use App\Models\Podcast;
use App\Models\PodcastEpisode;
use Carbon\Carbon;

class FetchPodcastInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'podcast:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch podcast info from listen notes.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $listenRepo = new ListenNotes;

        Podcast::all()->each(function($podcast) use($listenRepo) {
            $data = $listenRepo->getPodcast($podcast->listen_notes_id);
            foreach ($data['episodes'] as $episode) {
                PodcastEpisode::firstOrCreate([
                    'listen_notes_id' => $episode['id'],
                    'pub_date' => Carbon::createFromTimestamp(round($episode['pub_date_ms']/1000))->toDateTimeString(),
                    'podcast_id' => $podcast->id,
                ]);
            }
        });
    }
}
