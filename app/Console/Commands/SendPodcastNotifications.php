<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Aloha\Twilio\Twilio;
use Twilio\Exceptions\RestException as TwilioException;
use App\Logicians\Traits\LogsMessages;

use App\Models\Participant;
use App\Models\Podcast;
use App\Models\PodcastEpisode;
use App\Models\PodcastNotification;

use App\Repositories\ListenNotes;

use Log; 

class SendPodcastNotifications extends Command
{
    use LogsMessages;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'podcast:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications for podcast subscribers.';

    private $twilio;
    private $listenNotes;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->listenNotes = new ListenNotes;
        Podcast::all()->each(function($podcast) {
            if ($episode = $podcast->latestEpisode()) {
                $this->twilio = new Twilio(
                    config('logicians.twilio_account'), 
                    config('logicians.twilio_token'), 
                    $podcast->organization->service_number
                );
                return $this->sendFor($podcast, $episode);
            }
        });
    }

    private function sendFor(Podcast $podcast, PodcastEpisode $episode) {
        $subcribed = $podcast->subscriptions()
            ->where('active', 1)
            ->pluck('participant_id')
            ->all();

        $received = $podcast->notifications()
            ->where('episode_id', $episode->id)
            ->pluck('participant_id')
            ->all();

        $toSend = array_diff($subcribed, $received);

        if (!empty($toSend)) {
            $info = $this->listenNotes->getEpisode($episode->listen_notes_id);
            $message = "NEW EPISODE: ";
            $message .= $info['title'] . " ";
            $message .= $info['link'];
        }
        else if (empty($toSend)) {
            return;
        }

        foreach ($toSend as $participantId) {
            try {
                $participant = Participant::findOrFail($participantId);
                $log = $this->sendMessage($message, $participant);
                PodcastNotification::create([
                    'message_log_id' => $log->id,
                    'podcast_id' => $podcast->id,
                    'episode_id' => $episode->id,
                    'participant_id' => $participant->id,
                ]);
            }
            catch (\Exception $e) {
                \Log::error($e->getMessage());
                \Log::error('Could not send to participant ID ' . $participant->id . ' during sending.');
                $podcast->unsubscribe($participant);
                continue;
            }
        }
    }

    private function sendMessage($messages, Participant $participant) {
        if (!is_array($messages)) {
            $messages = array($messages);
        }

        foreach ($messages as $message) {
            try {
                $message = $this->twilio->message($participant->user_phone, $message);
                $log = $this->logOutgoingTwilioMessage($message, $participant);
                return $log;
            }
            catch (TwilioException $e) {
                Log::error($e->getMessage());
                throw new \Exception('Could not send message. Re-throwing exception to remove subscriber.');
            }
        }
    }
}
