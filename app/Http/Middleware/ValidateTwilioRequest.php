<?php

namespace App\Http\Middleware;

use Closure;
use Twilio\Security\RequestValidator;

class ValidateTwilioRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $requestValidator = new RequestValidator(env('TWILIO_TOKEN'));
      $isValid = $request->headers->has('X-Twilio-Signature');

      if ($isValid || env('APP_DEBUG') === true) {
        return $next($request);
      } else {
        abort(403, 'You are not Twilio');
      }
    }
}
