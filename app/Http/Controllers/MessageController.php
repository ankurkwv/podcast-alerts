<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Aloha\Twilio\Twilio;
use Twilio\Exceptions\RestException as TwilioException;

use App\Jobs\LogicianStartupJob;

use App\Models\Participant;
use App\Models\OptInRecord;
use App\Models\Trigger;

class MessageController extends Controller
{
    private $participant;
    private $used_trigger;

    public function __construct()
    {
    	$this->middleware('twilio')->only('index');
    }

    /**
     * Correctly route the incoming message.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$data = $this->getMessageData($request);

        list($driver, $method) = $this->determineDriver($data);
        $data['participant'] = $this->participant;
        $data['trigger'] = $this->used_trigger;

    	LogicianStartupJob::dispatch($driver, $data, $method);

    	return $this->safeTwilioResponse();
    }

    private function determineDriver($data) {
        $this->participant = Participant::with(['currentLogician'])
                    ->where('user_phone', $data['from'])
                    ->where('service_phone', $data['to'])
                    ->first() ?? $this->createParticipant($data['from'], $data['to']);
        
        if ($this->participant->currentLogician()->exists()) {
            return [$this->participant->currentLogician->driver, $this->participant->logician_method];
        }

        if ($trigger = $this->trigger($data['message'])) {
            $this->participant->current_campaign_id = $trigger->campaign_id;
            $this->participant->save();
            OptInRecord::generateFromTrigger($trigger, $this->participant, $data['sid']);
            return [$trigger->logician->driver, null];
        }

        if ($this->participant->currentCampaign()->exists()) {
            $logician_to_use = $this->participant->currentCampaign->fallbackLogician;
            return [$logician_to_use->driver, null];
        }

        return [config('logicians.global_fallback'), null];
        
    }

    private function trigger($msg) {
        foreach (Trigger::all() as $trigger) {
            $trigger_string = strtolower($trigger->trigger);
            $msg = strtolower($msg);

            if (levenshtein($trigger_string, $msg) < 2) {
                $this->used_trigger = $trigger;
                return $trigger;
            }
        }
        return false;
    }

    private function createParticipant($user, $bot) {
        $this->participant = new Participant;
        $this->participant->user_phone = $user;
        $this->participant->service_phone = $bot;
        $this->participant->save();
        return $this->participant;
    }

    private function safeTwilioResponse() {
    	return response("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Response/>", 200, ['Content-Type' => 'text/xml']);
    }

    private function getMessageData($request)
    {
        $text = trim($request['Body'], $character_mask = " \t\r\0\x0B" );

        return $data = [
            'sid' => $request['MessageSid'],
            'from' => $request['From'],
            'to' => $request['To'],
            'time' => microtime(true),
            'message' => $text,
        ];
    }
}